  (ns lamtest01.core
    (:use clojure.core)
      	(:require [serial-port :as sport])
		(:require [clojure.xml :as xml])
		(:require [lamina.core :as lamina])
    (:use [clojure.string :only (join split)]
      )

    )

    (def ch (lamina/channel))
    (def chx (lamina/channel))

    ; (defn loop-forever [f]
    ;   ; Since repeatedly is lazy, we wrap it in doall
    ;   (doall (repeatedly f)))

    (def loop-forever (comp doall repeatedly))
    (def ravenstr (ref ""))

    (defn producer1 []
    	(def port (sport/open "/dev/tty.usbserial" 115200))
    	(Thread/sleep 1000)
        (sport/on-byte port #(lamina/enqueue ch (char %)))		
        ;(lamina/enqueue ch "Hello World!")
          )

    (defn producer []
      (loop-forever
        (fn []
          (lamina/enqueue ch "Hello World!\n\r")
          (Thread/sleep 1000))))

    (defn consumer []
       (loop-forever
        (fn []          
			(let [rstr (str @ravenstr @(lamina/read-channel ch))]
				(dosync (ref-set ravenstr (str rstr)))
        ; search for contig xml
        ; pass to data xml.parse method as a string - get def records
        ; stuff into new channel (data channel)
        ; cut dat out of the original string - keep ravenstr short.
			)
      ;(println "RavenStr1: " (.split @ravenstr "\n<"))
      ;(println (str (join "<" (split @ravenstr #"\n<"))))
      (let [ 
        rawstr @ravenstr
        p1 (.indexOf @ravenstr "\n</" ) 
        p2 (.indexOf @ravenstr "\n<"  (+ p1 8))
        p3 (.indexOf @ravenstr "\n</" (+ p2 8))
        p4 (.indexOf @ravenstr "\n<"  (+ p3 8))
        p5 (.indexOf @ravenstr "\n</" (+ p4 8))
        p6 (.indexOf @ravenstr "\n<"  (+ p5 8))
        ]
        ;(println  "frst: " p1 p2 p3 p4 p5 p6 (.length @ravenstr))

        (if ( > p1 0) 
            (dosync (ref-set ravenstr (str (subs @ravenstr p1)))) 
        )
      )  
      (let [ 
        rawstr @ravenstr
        p1 (.indexOf @ravenstr "\n</" ) 
        p2 (.indexOf @ravenstr "\n<"  (+ p1 8))
        p3 (.indexOf @ravenstr "\n</" (+ p2 8))
        p4 (.indexOf @ravenstr "\n<"  (+ p3 8))
        p5 (.indexOf @ravenstr "\n</" (+ p4 8))
        p6 (.indexOf @ravenstr "\n<"  (+ p5 8))
        ]  
        (if ( < p1 p2 p3 p4 p5); we are synce'd to the xml tag start
          (do
            (lamina/enqueue chx (subs @ravenstr p2 p4)); enque the xml fragment
            ; (println (subs @ravenstr p1 (+ p1 10)))
            ; (println (subs @ravenstr p2 (+ p2 10)))
            ; (println (subs @ravenstr p3 (+ p3 10)))
            ; (println (subs @ravenstr p4 (+ p4 10)))
            ; (println (subs @ravenstr p5 (+ p5 10)))
            (println "next: " p1 p2 p3 p4 p5 p6 (.length @ravenstr))
        
            (println (subs @ravenstr p2 p4)); print the fragment
            (dosync (ref-set ravenstr (str (subs @ravenstr p4))))
          )
        ) 

        )
      ;(println (.indexOf @ravenstr "\n</") (.indexOf @ravenstr "\n</" 10))
      ;(println @ravenstr)

;(subs "Clojure" 1) 

      ; (let [cstring @ravenstr ]
      ;   (dosync (ref-set ravenstr (.split cstring "\n<"))))
      ;    	(println "RavenStr2: " @ravenstr)
        )
       )
    )
  ;(->> ch (lamina/map* xml/parse)(get :InstantaneousDemand :Demand )) print)

    

    (defn -main []
		;(-> (Thread. producer) .start)
      	;(println "P-Start")
; (def tstr "aneousDemand>
; <InstantaneousDemand>
;   <DeviceMacId>0x00158d0000000004</DeviceMacId>
;   <MeterMacId>0x00178d0000000004</MeterMacId>
;   <TimeStamp>0x185adc1d</TimeStamp>
;   <Demand>0x001738</Demand>
;   <Multiplier>0x00000001</Multiplier>
;   <Divisor>0x000003e8</Divisor>
;   <DigitsRight>0x03</DigitsRight>
;   <DigitsLeft>0x00</DigitsLeft>
;   <SuppressLeadingZero>Y</SuppressLeadingZero>
; <InstantaneousDemand>")

; (println (str (join "<" (split tstr #"\n<"))))
; (println tstr)



      	(-> (Thread. consumer) .start)
      	(println "Consumer-Start")
      	(-> (Thread. producer1) .start)
      	(println "Serial port producer-Start")
      	)

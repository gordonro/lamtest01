(defproject lamtest01 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
		[serial-port "1.1.2"]
		[lamina "0.5.2"]
		[org.clojure/data.xml "0.0.7"]
		 ]

  :main lamtest01.core
  )